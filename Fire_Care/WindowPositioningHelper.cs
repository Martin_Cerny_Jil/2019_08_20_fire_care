﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Fire_Care
{
    public class WindowPositioningHelper
    {
        private MainWindow myMaster;
        private int screenWidth = 0;
        private int screenHeight = 0;
        private WorkingArea4D myWorkingArea;
        private WorkingArea4D margins;

        public WindowPositioningHelper(MainWindow myMaster)
        {
            this.myMaster = myMaster;
            myWorkingArea = new WorkingArea4D();
            margins = new WorkingArea4D { Top = 27, Left = 50, Right = 10, Bottom = 50, };
        }

        public int ScreenWidth
        {
            get
            {
                if (screenWidth != 0) { return screenWidth; }
                else
                {
                    GetDesiredScreenDimensions();
                    return screenWidth;
                }
            }
            set => screenWidth = value;
        }

        public int ScreenHeight
        {
            get
            {
                if (screenHeight != 0) { return screenHeight; }
                else
                {
                    GetDesiredScreenDimensions();
                    return screenHeight;
                }
            }
            set => screenHeight = value;
        }

        private void GetDesiredScreenDimensions()
        {
            GetDesiredScreenDimensions3();
        }

        private void GetDesiredScreenDimensions1()
        {
            //throw new NotImplementedException();
            //WindowCheckingScreenSize checker = new WindowCheckingScreenSize(myWorkingArea);
            WindowCheckingScreenSize checker = new WindowCheckingScreenSize();
            checker.WindowState = WindowState.Maximized;

            checker.Show();
            checker.SaveDimensions();

            myWorkingArea.Height = checker.R.Height;
            myWorkingArea.Width = checker.R.Width;
            myWorkingArea.Top = checker.R.Top;
            myWorkingArea.Left = checker.R.Left;

            ScreenWidth = myWorkingArea.Width;
            ScreenHeight = myWorkingArea.Height;

            //checker.Close();
        }

        private void GetDesiredScreenDimensions2()
        {
            WindowCheckingScreenSize checker = new WindowCheckingScreenSize();
            checker.Show();
            checker.CountDimensions(myWorkingArea);
            checker.Close();
        }

        private void GetDesiredScreenDimensions3()
        {
            //throw new NotImplementedException();
            //WindowCheckingScreenSize checker = new WindowCheckingScreenSize(myWorkingArea);
            WindowCheckingScreenSize checker = new WindowCheckingScreenSize();
            checker.WindowState = WindowState.Maximized;

            checker.Show();
            checker.SaveDimensions2();

            myWorkingArea.Top = checker.R.Top;
            myWorkingArea.Left = checker.R.Left;
            myWorkingArea.Right = checker.R.Right;
            myWorkingArea.Bottom = checker.R.Bottom;

            myWorkingArea.Height = myWorkingArea.Bottom - myWorkingArea.Top;
            myWorkingArea.Width = myWorkingArea.Right - myWorkingArea.Left;

            ScreenWidth = myWorkingArea.Width;
            ScreenHeight = myWorkingArea.Height;

            checker.Close();
        }

        public void RestartWindowPosition()
        {
            if (myMaster.myWindowPosition == MyWindowPosition.TopLeft)
            {
                myMaster.Left = ScreenWidth - myMaster.Width - margins.Right;
                myMaster.Top = margins.Top;
                myMaster.myWindowPosition = MyWindowPosition.TopRight;
            }
            else
            {
                myMaster.Left = margins.Left;
                myMaster.Top = margins.Top;
                myMaster.myWindowPosition = MyWindowPosition.TopLeft;
            }
        }
    }

    public struct WorkingArea4D
    {
        public int Top;
        public int Left;
        public int Width;
        public int Height;
        public int Right;
        public int Bottom;
    }

    public enum MyWindowPosition
    {
        TopLeft,
        TopRight,
        Custom,
        Left,
        Right,
    }
}
