﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Fire_Care
{
    /// <summary>
    /// Interaction logic for WindowCheckingScreenSize.xaml
    /// </summary>
    public partial class WindowCheckingScreenSize : Window
    {
        private WorkingArea4D checkedSizeSavedHere;
        public WorkingArea4D R;

        //public WindowCheckingScreenSize(WorkingArea4D checkedSizeSavedHere)
        public WindowCheckingScreenSize()
        {
            InitializeComponent();
            /*
            this.checkedSizeSavedHere= checkedSizeSavedHere;
            SaveDimensions();
            Close();
            //this.Close();
            */
            checkedSizeSavedHere = new WorkingArea4D();
        }

        public void SaveDimensions()
        {
            checkedSizeSavedHere.Top = (int)Top + (int)SystemParameters.BorderWidth + (int)SystemParameters.CaptionHeight;
            checkedSizeSavedHere.Left = (int)Left + (int)SystemParameters.BorderWidth;
            checkedSizeSavedHere.Width = (int)Width - 2 * (int)SystemParameters.BorderWidth;
            checkedSizeSavedHere.Height = (int)Height - 2 * (int)SystemParameters.BorderWidth - (int)SystemParameters.CaptionHeight;
        }

        public void SaveDimensions2()
        {
            checkedSizeSavedHere.Top = (int)SystemParameters.WorkArea.Top;
            checkedSizeSavedHere.Left = (int)SystemParameters.WorkArea.Left;
            checkedSizeSavedHere.Right = (int)SystemParameters.WorkArea.Right;
            checkedSizeSavedHere.Bottom = (int)SystemParameters.WorkArea.Bottom;
            R = checkedSizeSavedHere;
        }

        internal void CountDimensions(WorkingArea4D myWorkingArea)
        {
            //myWorkingArea.

        }
    }
}
