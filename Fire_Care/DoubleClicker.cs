﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fire_Care
{
    public struct KeyLastTime
    {
        private int key;
        public DateTime pressed;

        public KeyLastTime(int key, DateTime now) : this()
        {
            this.key = key;
            this.pressed = now;
        }

        public override string ToString()
        {
            return key.ToString();
        }
    }

    public class DoubleClicker
    {
        private static int n_keys = 256;
        private List<KeyLastTime> keyLastTimes = new List<KeyLastTime>();
        private KeyLastTime[] keyLastTimesArray = new KeyLastTime[n_keys];

        private TimeSpan DoubleClickInterval;

        public DoubleClicker(int miliseconds)
        {
            DoubleClickInterval = new TimeSpan(10000 * miliseconds);
        }

        public void SetInterval(int miliseconds)
        {
            DoubleClickInterval = new TimeSpan(10000 * miliseconds);
        }
        public void SetInterval(TimeSpan interval)
        {
            DoubleClickInterval = interval;
        }

        public int Interval_miliseconds { get => (int)DoubleClickInterval.TotalMilliseconds; }

        /// <summary>
        /// Updates when this key has been pressed for the last time. 
        /// Returns True if this pressing has created a DoubleClick.
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public bool WasThisKeyPressed(int key)
        {
            bool result = false;
            DateTime now = DateTime.Now;
            KeyLastTime justPressed = new KeyLastTime(key, now);
            /*if (keyLastTimes.Contains(justPressed))            
            {
                int interval = now.Subtract(keyLastTimes.Where())
            }*/
            if (now.Subtract(keyLastTimesArray[key].pressed) < DoubleClickInterval)
            {
                result = true;
            }
            keyLastTimesArray[key].pressed = now;
            return result;
        }
    }
}
