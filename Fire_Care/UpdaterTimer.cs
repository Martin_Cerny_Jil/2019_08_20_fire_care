﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fire_Care
{
    class UpdaterTimer        
    {
        private System.Windows.Threading.DispatcherTimer displayUpdateTimer;
        private MainWindow window;       

        public UpdaterTimer(MainWindow window, int miliseconds)
        {           
            this.window = window;
            CreateTimerForUpdatingDisplay(miliseconds);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ms">Timer's interval in miliseconds</param>
        private void CreateTimerForUpdatingDisplay(int ms)
        {
            displayUpdateTimer = new System.Windows.Threading.DispatcherTimer();
            displayUpdateTimer.Tick += new EventHandler(UpdateTimer_Tick);
            displayUpdateTimer.Interval = new TimeSpan(0, 0, 0, 0, ms);
            displayUpdateTimer.Start();
        }

        private void UpdateTimer_Tick(object sender, EventArgs e)
        {
            window.UpdateDisplay();
        }
    }
}
