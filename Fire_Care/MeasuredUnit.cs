﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace Fire_Care
{
    public class MeasuredUnit
    {
        private static int ID =0;
        public string MU_Name { private set; get; }
        public DateTime LastUpdate;
        public TimeSpan IntervalOfChecking;
        public string DisplayedCaption;
        private string _buttonCaption;
        private MainWindow mainWindow;
        public static Thickness defaultMargin = new Thickness(10, 2, 10, 2);
        ///def original = 12
        public static int defaultFontSizeBig = 20;
        public static int defaultFontSizeSmall = 13;
        private SwitchColor Highlighting = SwitchColor.Normal;
        TimeSpan remains;

        System.Windows.Controls.StackPanel spThisMeasuredUnit;
        //TextBlock tbSizeMaker;
        TextBlock tbTime1;
        TextBlock tbTime2;
        TextBlock tbCaption;
        Button btnRefresh;
        
        public MeasuredUnit(MainWindow window, string name, TimeSpan intervalOfChecking)
        {
            mainWindow = window;

            string currentID = ID.ToString();
            ID++;
            MU_Name = name;
            IntervalOfChecking = intervalOfChecking;
            LastUpdate = DateTime.Now;
            DisplayedCaption = name;
            string s = "sp_" + name;

            //sp.Children.Remove((UIElement)this.FindName("Button0"));

            spThisMeasuredUnit = new System.Windows.Controls.StackPanel
            {
                Name = "sp_" + currentID,
                Tag = currentID,
                VerticalAlignment = VerticalAlignment.Top,
                Orientation = Orientation.Horizontal,
                HorizontalAlignment = HorizontalAlignment.Right,
                Background = MyColors.blackBackground,
            };
            tbCaption = new System.Windows.Controls.TextBlock
            {
                Name = "tb_Caption_" + currentID,
                Tag = currentID,
                TextWrapping = TextWrapping.Wrap,
                Text = DisplayedCaption,
                HorizontalAlignment = HorizontalAlignment.Left,
                VerticalAlignment = VerticalAlignment.Bottom,
                Foreground = MyColors.redFireText,
                Margin = defaultMargin,
                FontSize = defaultFontSizeBig,
            };
            /*
            tbSizeMaker = new TextBlock
            {
                Name = "tb_SizeMaker_" + currentID,
                Tag = currentID,
                //default font size: 12
                FontSize = tbCaption.FontSize + 5,
            };
            */
            tbTime1 = new TextBlock
            {
                Name = "tb_Time_" + currentID,
                Tag = currentID,
                TextWrapping = TextWrapping.Wrap,
                Text = RemainingTimeUntilNextCheck(),
                HorizontalAlignment = HorizontalAlignment.Left,
                VerticalAlignment = VerticalAlignment.Bottom,
                Foreground = MyColors.redFireText,
                Margin = new Thickness(defaultMargin.Left, defaultMargin.Top, 0, defaultMargin.Bottom),
                FontSize = defaultFontSizeBig,
            };
            tbTime2 = new TextBlock
            {
                Name = "tb_Time_" + currentID,
                Tag = currentID,
                TextWrapping = TextWrapping.Wrap,
                Text = RemainingTimeUntilNextCheck(),
                HorizontalAlignment = HorizontalAlignment.Left,
                VerticalAlignment = VerticalAlignment.Bottom,
                Foreground = MyColors.redFireText,
                Margin = new Thickness(0, defaultMargin.Top, defaultMargin.Right, defaultMargin.Bottom),
                FontSize = defaultFontSizeSmall,
            };
            btnRefresh = new Button
            {
                Name = "btn_Refresh_" + currentID,
                Tag = currentID,
                Content = "Set",
                VerticalAlignment = VerticalAlignment.Bottom,
                Foreground = MyColors.redFireText,
                Background = MyColors.darkButtonBackground,
                Margin = defaultMargin,
                FontSize = defaultFontSizeBig,
                MinWidth = 70,
            };
            btnRefresh.Click += BtnRefresh_Click;

            spThisMeasuredUnit.Children.Add(tbCaption);
            spThisMeasuredUnit.Children.Add(tbTime1);
            spThisMeasuredUnit.Children.Add(tbTime2);
            spThisMeasuredUnit.Children.Add(btnRefresh);
            mainWindow.wrapPanelDisplay.Children.Add(spThisMeasuredUnit);
        }

        public string ButtonCaption
        {
            get => _buttonCaption; set
            {
                _buttonCaption = value;
                //add buttton !!!
            }
        }

        private void BtnRefresh_Click(object sender, RoutedEventArgs e)
        {
            RestartCountdown();
        }

        public void RestartCountdown()
        {
            LastUpdate = DateTime.Now;
            //if (Highlighting != SwitchColor.Normal)
            UpdateDisplay();
        }

        public string RemainingTimeUntilNextCheck()
        {
            //string s = ((int)remains.TotalHours).ToString() + ":" + remains.Minutes.ToString() + ":" + remains.Seconds.ToString();
            //s += " // " + remains.ToString();
            //s = string.Format("{0:HH:mm:ss}", remains);  //--asi DateTime format
            return remains.ToString(@"hh\:mm");
        }

        public string RemainingTimeUntilNextCheck2()
        {   
            return remains.ToString(@"\:ss\,f");
        }

        private void Repaint()
        {
            /*
            List<Type> list = new List<Type>();
            list.Add(TextBlock);
            list.Add(Button);
            */
            switch (Highlighting)
            {
                case SwitchColor.ToHighlight:
                    foreach (var control in spThisMeasuredUnit.Children.OfType<TextBlock>())
                        control.Foreground = MyColors.redFireTextHighlight;

                    foreach (var control in spThisMeasuredUnit.Children.OfType<Button>())
                        control.Foreground = MyColors.redFireTextHighlight;
                    /*
                    foreach (Control e in spThisMeasuredUnit.Children)
                    {
                        //foreach (var e in spThisMeasuredUnit.Children.OfType<TextBlock>())
                        //foreach (PaintableObject e in spThisMeasuredUnit.Children)
                        //    foreach (System.Windows.Controls e in spThisMeasuredUnit.Children)
                        TextBox tb = e as TextBox;
                        tb.Foreground = MyColors.redFireTextHighlight;
                    }
                    */
                    /*
                    tbCaption.Foreground = MyColors.redFireTextHighlight;
                    tbTime1.Foreground = MyColors.redFireTextHighlight;
                    tbTime2.Foreground = MyColors.redFireTextHighlight;
                    */
                    Highlighting = SwitchColor.Highlighted;
                    break;

                case SwitchColor.ToNormal:
                    foreach (var control in spThisMeasuredUnit.Children.OfType<TextBlock>())
                        control.Foreground = MyColors.redFireText;

                    foreach (var control in spThisMeasuredUnit.Children.OfType<Button>())
                        control.Foreground = MyColors.redFireText;
                    Highlighting = SwitchColor.Normal;
                    break;
            }       
        }

        public void UpdateDisplay()
        {
            remains = LastUpdate.Add(IntervalOfChecking).Subtract(DateTime.Now);
            if (remains.Ticks < 0)
            {
                if (Highlighting == SwitchColor.Normal)
                    Highlighting = SwitchColor.ToHighlight;
            }
            else
            {
                if (Highlighting == SwitchColor.Highlighted)
                    Highlighting = SwitchColor.ToNormal;
            }
            Repaint();
            tbTime1.Text = RemainingTimeUntilNextCheck();
            tbTime2.Text = RemainingTimeUntilNextCheck2();            
        }
    }

    public enum SwitchColor
    {
        Normal,
        ToHighlight,
        Highlighted,
        ToNormal,
    }
}
