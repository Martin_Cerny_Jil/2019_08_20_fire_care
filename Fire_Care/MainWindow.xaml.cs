﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Fire_Care
{
    /// <summary>
    /// Interakční logika pro MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private DoubleClicker doubleClicker = new DoubleClicker(500);
        private static int rightMargin = 50;
        private static int topMargin = rightMargin;
        private MeasuredUnit loadingFireplace;
        private WindowPositioningHelper positioningHelper;
        public MyWindowPosition myWindowPosition = MyWindowPosition.TopLeft;

        public MainWindow()
        {
            InitializeComponent();
            //Left = System.Windows.Forms.SystemInformation.VirtualScreen.Width - this.Width - rightMargin;

            /*
             * replaced by positionHelper
            Left = 1920 - 1 - this.Width - rightMargin;
            Top = topMargin;
            */
            loadingFireplace = new MeasuredUnit(this, "Přiložit", new TimeSpan(0, 0, 5));//(0, 5, 0));
            UpdaterTimer updaterTimer = new UpdaterTimer(this, 50);
            positioningHelper = new WindowPositioningHelper(this);
            positioningHelper.RestartWindowPosition();
            SetupTextblocks();
        }

        private void Window_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            CloseThisSomehow();
        }

        private void Window_KeyDown(object sender, KeyEventArgs e)
        {
            //MessageBox.Show(((int)e.Key).ToString());

            if (e.Key == Key.Escape)
            {
                MessageBoxResult messageBoxResult = System.Windows.MessageBox.Show(
                    "Close this application?", "Closing!?!", MessageBoxButton.YesNoCancel);
                switch (messageBoxResult)
                {
                    case MessageBoxResult.Yes:
                        CloseThisSomehow();
                        break;
                    case MessageBoxResult.No:
                        positioningHelper.RestartWindowPosition();
                        break;
                }
                //if (messageBoxResult == MessageBoxResult.OK) { CloseThisSomehow(); }
            }

            // Ctrl left or right
            if ((int)e.Key == 118 ^ (int)e.Key == 119)
            {
                //DateTime now = DateTime.Now;
                if (doubleClicker.WasThisKeyPressed((int)e.Key))
                {
                    //MessageBox.Show("Ctrl Doubleclick");
                    CtrlDoubleclick();
                }
            }
        }

        private void CloseThisSomehow()
        {
            Application.Current.Shutdown();
        }

        internal void UpdateDisplay()
        {
            tbCurrentTime.Text = DateTime.Now.ToString("HH:mm:ss");

            //Measured unit:
            loadingFireplace.UpdateDisplay();
            //wrapPanelDisplay.Children[]
            //tb_TimeRemaining.Text = loadingFireplace.RemainingTimeUntilNextCheck();
        }

        private void SetupTextblocks()
        {
            tbWindowCaption.FontSize = MeasuredUnit.defaultFontSizeBig;
            tbWindowCaption.Background = MyColors.darkButtonBackground;
            sp01CurrentTime.HorizontalAlignment = HorizontalAlignment.Stretch;
            tbCurrentTimeCaption.Foreground = MyColors.grayClockText;
            tbCurrentTimeCaption.FontSize = MeasuredUnit.defaultFontSizeSmall;
            tbCurrentTimeCaption.Margin = MeasuredUnit.defaultMargin;
            tbCurrentTimeCaption.VerticalAlignment = VerticalAlignment.Bottom;
            tbCurrentTime.Foreground = MyColors.lightGrayClockText;
            tbCurrentTime.FontSize = MeasuredUnit.defaultFontSizeBig;
            tbCurrentTime.Margin = MeasuredUnit.defaultMargin;
            tbCurrentTime.VerticalAlignment = VerticalAlignment.Bottom;
            //sp.Children.Remove((UIElement)this.FindName("Button0"));
            //https://stackoverflow.com/questions/55546033/c-sharp-how-to-recursively-delete-objects-and-its-children
            wrapPanelDisplay.Children.Remove((UIElement)this.FindName("spExample"));            
        }

        private void CtrlDoubleclick()
        {
            loadingFireplace.RestartCountdown();
            //BtnUpdate_Click(this, null);
        }

        private void BtnUpdate_Click(object sender, RoutedEventArgs e)
        {

        }
    }
}
