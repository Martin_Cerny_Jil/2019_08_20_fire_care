﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows;

namespace Fire_Care
{
    interface IPaintingForeground
    {
        Brush Foreground { get; set; }
    }

    //public class PaintableObject : FrameworkElement, IPaintingForeground
    public class PaintableObject : IPaintingForeground
    {
        public Brush Foreground { get; set; }
    }
}
