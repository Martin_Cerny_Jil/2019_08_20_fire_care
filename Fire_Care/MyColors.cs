﻿using System.Windows.Media;

namespace Fire_Care
{
    public static class MyColors
    {
        public static Brush redFireText = new SolidColorBrush(Color.FromArgb(255, 220, 100, 20));
        public static Brush lightGrayClockText = new SolidColorBrush(Color.FromArgb(255, 190, 180, 180));
        public static Brush grayClockText = new SolidColorBrush(Color.FromArgb(255, 160, 150, 150));
        public static Brush redFireTextHighlight = new SolidColorBrush(Color.FromArgb(255, 255, 23, 0));
        public static Brush darkTransparentBackground = new SolidColorBrush(Color.FromArgb(200, 0, 0, 0));
        public static Brush darkButtonBackground = new SolidColorBrush(Color.FromArgb(255, 0, 20, 5));
        public static Brush blackBackground = new SolidColorBrush(Color.FromArgb(255, 0, 0, 0));
    }
}
